# node-js-chat

This is a slightly modified version of the source code for a 
very simple private chat example. The original example was used 
for the [Getting Started](http://socket.io/get-started/chat/) 
guide of the Socket.IO website.

Please refer to it to learn how to run this application.

run 'npm install' in the home directory to download all the
node modules needed to run this project.
